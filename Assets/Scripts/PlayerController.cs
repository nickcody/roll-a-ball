﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	public Rigidbody rb;
	public float speed;
	public Text scoreText;
	public Text timeText;

	private float timeOfFirstMove;
	private bool gameOver;
	private bool gameStarted;

	private int count;

	void Start()
	{
		rb = GetComponent<Rigidbody>();
		count = 0;
		timeOfFirstMove = -1.0f;
		gameStarted = false;
		gameOver = false;
		timeText.text = "Hit spacebar!";
		UpdateCount (0);
	}

	// Update is called once per frame
	void FixedUpdate () {

		if (!gameStarted && Input.GetKeyDown(KeyCode.Space)) {
			gameStarted = true;
			timeOfFirstMove = Time.time;
		}

		if (gameOver && Input.GetKeyDown(KeyCode.Space)) {
			SceneManager.LoadScene("Main");
		}

		if (Input.GetKey (KeyCode.Escape)) {
			Application.Quit ();
		}


		if (gameStarted && !gameOver ) {
			timeText.text = System.Math.Round((Time.time - timeOfFirstMove), 2).ToString ();
			float moveHorizontal = Input.GetAxis ("Horizontal");
			float moveVertical = Input.GetAxis ("Vertical");
			rb.AddForce (new Vector3 (moveHorizontal, 0, moveVertical) * speed);
		}

	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("Pick Up")) {
			other.gameObject.SetActive (false);
			UpdateCount (1);
		}
	}

	void UpdateCount(int amt) {
		count += amt;
		if (count == 10) {
			scoreText.text = "You Won!";
			gameOver = true;
		} else {
			scoreText.text = "Count: " + count.ToString ();
		}
	}
}
